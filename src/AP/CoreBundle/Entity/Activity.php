<?php

namespace AP\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="AP\CoreBundle\Repository\ActivityRepository")
 */
class Activity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="RPE", type="smallint")
     */
    private $rPE;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duration", type="time")
     */
    private $duration;

    /**
     * @var int
     *
     * @ORM\Column(name="averageHR", type="smallint", nullable=true)
     */
    private $averageHR;

    /**
     * @var int
     *
     * @ORM\Column(name="maxHR", type="smallint", nullable=true)
     */
    private $maxHR;

    /**
     * @var int
     *
     * @ORM\Column(name="endHR", type="smallint", nullable=true)
     */
    private $endHR;

    /**
     * @var int
     *
     * @ORM\Column(name="endHROneMin", type="smallint", nullable=true)
     */
    private $endHROneMin;

    /**
     * @var bool
     *
     * @ORM\Column(name="sucess", type="boolean")
     */
    private $sucess;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Activity
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set rPE
     *
     * @param integer $rPE
     *
     * @return Activity
     */
    public function setRPE($rPE)
    {
        $this->rPE = $rPE;

        return $this;
    }

    /**
     * Get rPE
     *
     * @return int
     */
    public function getRPE()
    {
        return $this->rPE;
    }

    /**
     * Set duration
     *
     * @param \DateTime $duration
     *
     * @return Activity
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return \DateTime
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set averageHR
     *
     * @param integer $averageHR
     *
     * @return Activity
     */
    public function setAverageHR($averageHR)
    {
        $this->averageHR = $averageHR;

        return $this;
    }

    /**
     * Get averageHR
     *
     * @return int
     */
    public function getAverageHR()
    {
        return $this->averageHR;
    }

    /**
     * Set maxHR
     *
     * @param integer $maxHR
     *
     * @return Activity
     */
    public function setMaxHR($maxHR)
    {
        $this->maxHR = $maxHR;

        return $this;
    }

    /**
     * Get maxHR
     *
     * @return int
     */
    public function getMaxHR()
    {
        return $this->maxHR;
    }

    /**
     * Set endHR
     *
     * @param integer $endHR
     *
     * @return Activity
     */
    public function setEndHR($endHR)
    {
        $this->endHR = $endHR;

        return $this;
    }

    /**
     * Get endHR
     *
     * @return int
     */
    public function getEndHR()
    {
        return $this->endHR;
    }

    /**
     * Set endHROneMin
     *
     * @param integer $endHROneMin
     *
     * @return Activity
     */
    public function setEndHROneMin($endHROneMin)
    {
        $this->endHROneMin = $endHROneMin;

        return $this;
    }

    /**
     * Get endHROneMin
     *
     * @return int
     */
    public function getEndHROneMin()
    {
        return $this->endHROneMin;
    }

    /**
     * Set sucess
     *
     * @param boolean $sucess
     *
     * @return Activity
     */
    public function setSucess($sucess)
    {
        $this->sucess = $sucess;

        return $this;
    }

    /**
     * Get sucess
     *
     * @return bool
     */
    public function getSucess()
    {
        return $this->sucess;
    }
}

