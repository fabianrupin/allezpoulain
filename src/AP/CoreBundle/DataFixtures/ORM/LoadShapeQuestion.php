<?php

namespace AP\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AP\CoreBundle\Entity\ShapeQuestion;

class LoadShapeQuestion implements FixtureInterface
{
  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    // Liste des noms de catégorie à ajouter
    $titles = array(
      "J'ai regardé la TV",
      "J'ai ri",
      "J'ai été de mauvaise humeur",
      "Je me suis senti physiquement détendu",
      "J'ai eu un bon moral",
			"J'ai eu des difficultés de concentrations",
			"Je me suis inquiété à propos de problèmes non résolus",
			"J'ai passé du bon temps avec des amis",
			"J'ai eu mal à la tête",
			"J'ai été épuisé après le travail",
			"J'ai réussi ce que j'ai entrepris",
			"Je me suis senti mal à l'aise",
			"J'ai été ennuyé par d'autres personnes",
			"J'ai connu une baisse de régime",
			"J'ai eu un sommeil de qualité",
			"J'en ai eu marre de tout",
			"J'ai été de bonne humeur",
			"J'ai été très fatigué",
			"J'ai eu un sommeil qui ne m'a pas reposé",
			"Je me suis senti mal",
			"Je me suis senti \"comme si je pouvais tout faire\"",
			"J'ai été boulversé",
			"Je n'ai pas pris de décision",
			"J'ai pris d'importantes décisions",
			"Je me suis senti sous pression",
			"Certaines parties de mon corps ont été douloureuses",
			"Je n'ai pas pu récupérer durant les pauses",
			"J'ai été convaincu que je pourvais atteindre mes objectifs au cours de l'activité sportive",
			"J'ai bien récupéré physiquement",
			"Je me suis senti épuisé/exténué par ma pratique sportive",
			"J'ai accompli des choses importantes dans ma pratique sportive",
			"Je me suis préparé mentalement pour l'activité sportive",
			"Mes muscles étaient douloureux ou tendus durant l'effort",
			"J'ai eu l'impression qu'il y avait trop peu de pauses",
			"j'ai été convaincu que je pouvais atteindre mes objectfs à tout moment",
			"J'ai géré de manière efficace les problèmes avec mes coéquipiers",
			"J'ai été dans une bonne condition physique",
			"Je me suis \"dépassé\" pendant l'effort",
			"Je me suis senti émotionnellmeent vidé par l'activité sportive",
			"J'ai ressenti des douleurs musculaires après l'effort",
			"J'ai eu la conviction que j'effectuais de bonnes performances",
			"On a trop exigé de moi durant les pauses",
			"Je me suis préparé mentalement avant l'activité sportive",
			"J'ai voulu arrêter mon sport",
			"Je me suis senti plein d'énergie",
			"J'ai facilement compris ce que mes coéquipiers ressentaient", 
			"J'ai été convaincu de m'être bien entraîné",
			"Les pauses n'étaient pas aux bons moments",
			"Je me suis vulnérable face aux blessures",
			"Je me suis fixé des objectifs personnels au cours de l'activité sportive",
			"Je me suis senti physiquement fort",
			"Je me suis senti frustré/insatisfait par mon sport",
			"J'ai géré calmement les problèmes émotionnels en lien avec mon sport"
    );

    foreach ($titles as $title) {
      // On crée la question
      $shapeQuestion = new ShapeQuestion();
      $shapeQuestion->setName($name);

      // On la persiste
      $manager->persist($shapeQuestion);
    }

    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }
}