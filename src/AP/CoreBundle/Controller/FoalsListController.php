<?php

namespace AP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FoalsListController extends Controller
{
    public function indexAction()
    {
        return $this->render('APCoreBundle:FoalsList:index.html.twig');
    }
}
