<?php

namespace AP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FoalController extends Controller
{
    public function indexAction()
    {
        return $this->render('APCoreBundle:Foal:index.html.twig');
    }
}
