<?php

namespace AP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PlanController extends Controller
{
    public function indexAction()
    {
        return $this->render('APCoreBundle:Plan:index.html.twig');
    }
}
