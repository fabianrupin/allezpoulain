<?php

namespace AP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ShapeController extends Controller
{
    public function indexAction()
    {
        return $this->render('APCoreBundle:Shape:index.html.twig');
    }
}
