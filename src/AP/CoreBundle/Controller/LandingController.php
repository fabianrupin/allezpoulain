<?php

namespace AP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LandingController extends Controller
{
    public function indexAction()
    {
        return $this->render('APCoreBundle:Landing:index.html.twig');
    }
}
