<?php

namespace AP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CoachController extends Controller
{
    public function indexAction($foalName)
    {
        return $this->render('APCoreBundle:Coach:index.html.twig', array(
				'foalName' => $foalName));
    }
}
